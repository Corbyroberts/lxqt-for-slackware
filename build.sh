#!/bin/bash

for i in \
lxqt-build-tools \
libqtxdg \
liblxqt \
libsysstat \
libfm-qt \
lxqt-themes \
lxqt-qtplugin \
obconf-qt \
pavucontrol-qt \
qtermwidget \
lximage-qt \
lxqt-about \
lxqt-admin \
lxqt-archiver \
lxqt-config \
lxqt-globalkeys \
lxqt-notificationd \
lxqt-openssh-askpass \
lxqt-policykit \
lxqt-session \
lxqt-sudo \
pcmanfm-qt \
qterminal \
lxqt-panel \
lxqt-powermanagement \
lxqt-runner \
screengrab \
; do
cd $i || exit 1
./$i.SlackBuild || exit 1
cd ..
done
